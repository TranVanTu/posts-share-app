const express = require('express');
const { ensureAuth, ensureGuest } = require('../middleware/auth');
const router = express.Router();
const Story = require('../models/Story');
// login page
router.get('/', (req, res) => {
  res.render('login', {
    layout: 'login'
  })
})


// dash board
router.get('/dashboard', async (req, res) => {
  try
  {
    const stories = await Story.find({
      user: req.user.id
    }).lean();
    res.render('dashboard', {
      name: req.user.displayName,
      stories
    })
  } catch (err)
  {
    res.render('error/500');
  }

})

module.exports = router; 