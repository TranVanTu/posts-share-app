const express = require('express');
const { ensureAuth, ensureGuest } = require('../middleware/auth');
const router = express.Router();
const Story = require('../models/Story');
// add page
router.get('/add', (req, res) => {
  res.render('stories/add');
});

// process post request
router.post('/', async (req, res) => {
  try
  {
    req.body.user = req.user.id
    await Story.create(req.body);
    res.redirect('/dashboard');
  } catch (err)
  {
    console.log(err);
    res.render('error/500');
  }
})

// user stories page
router.get('/user/:userId', async (req, res) => {
  try
  {
    const stories = await Story.find({
      user: req.params.userId,
      status: 'public'
    }).populate('user').lean()
    res.render('stories/index', {
      stories
    })
  } catch (error)
  {
    console.log(error);
    res.render('error/404')
  }
});

// get stories 
router.get('/', async (req, res) => {
  try
  {
    const stories = await Story.find({ status: 'public' })
      .populate('user')
      .sort({
        createdAt: 'desc'
      }).lean();
    res.render('stories/index', {
      stories
    })
  } catch (err)
  {
    res.render('error/500')
  }
});

// get single story 
router.get('/:id', async (req, res) => {
  try
  {
    const story = await Story.findById(req.params.id)
      .populate('user')
      .lean();
    if (story)
    {
      res.render('stories/show', {
        story
      })
    } else
    {
      res.render('error/404')
    }

  } catch (err)
  {
    res.render('error/500')
  }
});


// edit page
router.get('/edit/:id', async (req, res) => {
  const story = await Story.findOne({
    _id: req.params.id
  }).lean()
  if (!story)
  {
    return res.render('error/404')
  }
  if (story.user != req.user.id)
  {
    res.redirect('/stories')
  } else
  {
    res.render('stories/edit', { story });
  }
});

// update story page
router.put('/:id', async (req, res) => {
  try
  {
    let story = await Story.findById(req.params.id).lean();
    if (!story)
    {
      res.render('error/404')
    }
    if (story.user != req.user.id)
    {
      res.redirect('/stories')
    } else
    {
      story = await Story.findOneAndUpdate({
        _id: req.params.id
      },
        req.body,
        {
          new: true,
          runValidators: true
        }
      )
      res.redirect('/dashboard');
    }
  } catch (error)
  {
    res.render('error/500')
  }
});


//delete story
router.delete('/:id', async (req, res) => {
  try
  {
    await Story.remove({ _id: req.params.id });
    res.redirect('/dashboard')
  } catch (error)
  {
    res.render('error/500')
  }
})

module.exports = router; 