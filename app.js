const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const mongoose = require('mongoose');
const exphbs = require('express-handlebars');
const connectDB = require('./config/db');
const path = require('path');
const passport = require('passport');
const session = require('express-session');
const methodOverride = require('method-override');
const MongoStore = require('connect-mongo')(session);
const hbs = require('handlebars');
// load config
dotenv.config({ path: './config/config.env' });

// connect with db
connectDB();

const app = express();

// body parser

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
// method override 
app.use(methodOverride(function (req, res) {
  if (req.body && typeof req.body === 'object' && '_method' in req.body)
  {
    // look in urlencoded POST bodies and delete it
    let method = req.body._method;
    delete req.body._method;
    return method;
  }
}))

// passport config
require('./config/passport')(passport);


// logging
if (process.env.NODE_ENV === 'development')
{
  app.use(morgan('dev'));
}

// register handle bars

const { formatDate, truncate, stripTags, editIcon, select } = require('./helpers/hbs');
// const { delete } = require('./routes');

// handle bars

app.engine('.hbs', exphbs(
  {
    helpers: {
      formatDate,
      truncate,
      stripTags,
      editIcon,
      select
    },
    defaultLayout: 'main',
    extname: '.hbs'
  }));

app.set('view engine', '.hbs');

// sessions

app.use(
  session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: mongoose.connection })
  })
);

// static folder

app.use(express.static(path.join(__dirname, 'public')));

// set the passport middleware

app.use(passport.initialize());
app.use(passport.session());

// set global var
app.use(function (req, res, next) {
  res.locals.user = req.user || null;
  next()
})

// routes
app.use('/', require('./routes/index'));
app.use('/auth', require('./routes/auth'));
app.use('/stories', require('./routes/stories'));

const PORT = process.env.PORT || 3000;

app.listen(PORT, console.log(`server running in${process.env.NODE_ENV} mode on PORT ${PORT}`));